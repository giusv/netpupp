const chromium = require('chrome-aws-lambda');
const http = require('http');
exports.handler = async (event, context) => {

    var params = JSON.parse(event.body);
    //get IP
    var lambdaIP = null;
    http.get({'host': 'api.ipify.org', 'port': 80, 'path': '/'}, function(resp) {
        resp.on('data', function(ip) {
            lambdaIP = ip.toString();
        });
        resp.on('error', function(err) {
            console.log(err);
        });
    });
    // launch a headless browser
    // const browser = await chromium.puppeteer.launch({
    //     args: chromium.args,
    //     defaultViewport: chromium.defaultViewport,
    //     executablePath: await chromium.executablePath,
    //     headless: chromium.headless,
    // });
    
    const browser = await chromium.puppeteer.launch({
        args: chromium.args.concat([`--user-agent=${params.ua}`]),
        headless: false,
        executablePath: await chromium.executablePath,
        defaultViewport: chromium.defaultViewport
    });

    const page = await browser.newPage()
    found = false
    data = {}
    await page.on('response', async response => {
        if (response.request().url().includes('/getEquityOptionsHistory')) {
            if (!found) {
                //console.log('\nWe got one!: ', response.url())
                //console.log(response.request().headers())
                found = true
                data = await response.json()
                data.ip = lambdaIP
            }
        }
    })
    await page.goto(
        params.url, { waitUntil: 'networkidle0' }
    )
    return data;







    // const pageToScreenshot = JSON.parse(event.body).pageToScreenshot;

    // if (!pageToScreenshot) return {
    //     statusCode: 400,
    //     body: JSON.stringify({ message: 'Page URL not defined' })
    // }

    // const browser = await chromium.puppeteer.launch({
    //     args: chromium.args,
    //     defaultViewport: chromium.defaultViewport,
    //     executablePath: await chromium.executablePath,
    //     headless: chromium.headless,
    // });
    
    // const page = await browser.newPage();

    // await page.goto(pageToScreenshot, { waitUntil: 'networkidle2' });

    // const screenshot = await page.screenshot({ encoding: 'binary' });

    // await browser.close();
  
    // return {
    //     statusCode: 200,
    //     body: JSON.stringify({ 
    //         message: `Complete screenshot of ${pageToScreenshot}`, 
    //         buffer: screenshot 
    //     })
    // }

}
